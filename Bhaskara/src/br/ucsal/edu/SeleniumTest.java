package br.ucsal.edu;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.jbehave.core.annotations.AfterStories;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class SeleniumTest {
	private WebDriver driver;

	public SeleniumTest(WebDriver driver) {
		this.driver = driver;
	}

	@Given("estou na calculadora de bhaskara")
	public void givenEstouNaListaDeCompras() {
		if (driver == null) {
			System.setProperty("webdriver.chrome.driver", "C:\\chromedriverTED\\chromedriver.exe");
			driver = new ChromeDriver();

		}
		driver.get("http://localhost:8080/Bhaskara/index.jsp");

	}

	@When("informo o valor de A como $valorA")
	public void InformoValorDeA(String valorA) throws InterruptedException {
		Thread.sleep(300);
		driver.findElement(By.id("valorA")).click();
		Thread.sleep(300);
		driver.findElement(By.id("valorA")).sendKeys(valorA);
	}

	@When("informo o valor de B como $valorB")
	public void InformoValorDeB(String valorB) throws InterruptedException {
		Thread.sleep(300);
		driver.findElement(By.id("valorB")).click();
		Thread.sleep(300);
		driver.findElement(By.id("valorB")).sendKeys(valorB);
	}

	@When("informo o valor de C como $valorC")
	public void InformoValorDeC(String valorC) throws InterruptedException {
		Thread.sleep(300);
		driver.findElement(By.id("valorC")).click();
		Thread.sleep(300);
		driver.findElement(By.id("valorC")).sendKeys(valorC);
	}

	@When("confirmo o cálculo")
	public void ConfirmarCalculo() throws InterruptedException {
		Thread.sleep(300);
		driver.findElement(By.xpath("/html/body/form/button")).click();
	}

	@Then("terei o valor de Delta igual a $valorDeDelta")
	public void ValorDeDelta(String valorDeDelta) throws InterruptedException {
		Thread.sleep(300);
		String Delta = driver.findElement(By.xpath("/html/body/div/ul[1]/li/span")).getText();
		valorDeDelta = "Delta: " + valorDeDelta;
		assertEquals(valorDeDelta, Delta);
	}

	@AfterStories
	public void FecharNavegador() throws InterruptedException {
		Thread.sleep(200);
		driver.quit();
		
	}
}
