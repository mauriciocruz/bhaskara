Título: Delta

Cenário: Delta Positivo
Dado que estou na calculadora de bhaskara
Quando informo o valor de A como -4
E informo o valor de B como 3
E informo o valor de C como 5
E confirmo o cálculo
Então terei o valor de Delta igual a 89.0 

Cenário: Delta Negativo
Dado que estou na calculadora de bhaskara
Quando informo o valor de A como 4
E informo o valor de B como 5
E informo o valor de C como 3
E confirmo o cálculo
Então terei o valor de Delta igual a -23.0