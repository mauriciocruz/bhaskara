package br.ucsal.edu;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;
import br.uninove.bean.CalculadoraBean;


public class JUnitTest {

	CalculadoraBean calculadora = new CalculadoraBean();
	
	@Test
	public void TestarGetDelta() {
		
		calculadora.setA("-4");
		calculadora.setB("4");
		calculadora.setC("5");
		
		Double Delta = calculadora.getDelta();
		assertEquals(96.0, Delta);
	}
	
	@Test
	public void TestarGetRaizPositiva() {
		
		calculadora.setA("-4");
		calculadora.setB("4");
		calculadora.setC("5");
		
		String RaizPositiva = calculadora.getRaizPositiva();
		assertEquals("-0.7247448713915889", RaizPositiva);
	}
	
	@Test
	public void TestarGetRaizNegativa() {
		
		calculadora.setA("-4");
		calculadora.setB("4");
		calculadora.setC("5");
		
		String RaizNegativa = calculadora.getRaizNegativa();
		assertEquals("1.724744871391589", RaizNegativa);
	}
	
}
